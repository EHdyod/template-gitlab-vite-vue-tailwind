import { createWebHistory, createRouter } from "vue-router";
import Home from "../views/Home.vue";
import About from "../views/About.vue";
import PathsNames from "./paths-name";
import RoutesNames from "./routes-name";

const routes = [
    {
        path: "/",
        redirect: PathsNames["HOME"],
    },
    {
        path: PathsNames["HOME"],
        name: RoutesNames["HOME"],
        component: Home,
    },
    {
        path: PathsNames["ABOUT"],
        name: RoutesNames["ABOUT"],
        component: About
    },
    {
        path: "/:catchAll(.*)",
        redirect: PathsNames["HOME"]
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;
